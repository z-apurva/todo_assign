var dbLogin = require('../models/login')
var jwt = require('jsonwebtoken')
exports.login = (req,res)=>{
    if(!req.body.Emailid||!req.body.Password){
        res.json({
            sucess:false,
            msg:"please enter all the detail"
        })
    }else{
        dbLogin.findOne({Emailid:req.body.Emailid},(err,LoginData)=>{
            if(err){
                res.json({
                    sucess:false,
                    msg:"Try Agian"
                })
            }else if(!LoginData||LoginData==null)
            {
                res.json({
                    sucess:false,
                    msg:"Please register first"
                })
            }else if(LoginData.Password!=req.body.Password)
            {
                res.json({
                    msg:"Password incoorect"
                })
            }else {
                var tokenData = {
                    _id:LoginData._id,
                    Name:LoginData.Name
                }
                var token = jwt.sign(tokenData,'secret')
                res.json({
                    sucess: true,
                    msg:"Sucessfully Login",
                    token: token
                })
            }
        })
    }
}