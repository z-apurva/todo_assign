var express = require("express")
var app = express.Router()
var tokenVerify = require('./tokenVerify')


var addtask = require('./Task/addtasks')
app.post('/addtask',tokenVerify,addtask)

var gettask = require('./Task/gettasks')
app.get('/gettask',tokenVerify,gettask.gettask)

// var gettask = require('./Task/gettasks')
// app.get('/gettask',tokenVerify,gettask)

var updatetask = require('./Task/updatetasks')
app.post('/updatetask',tokenVerify,updatetask)

var deletetask = require('./Task/deletetasks')
app.delete('/deletetask',deletetask.deletetask)

// var deletetask = require('./Task/deletetasks')
// app.delete('/deletetask',tokenVerify,deletetask)



var adduser = require('./User/addusers')
app.post('/adduser',tokenVerify,adduser)

var getuser = require('./User/getuser')
app.get('/getuser',tokenVerify,getuser.getuser)


// var getuser = require('./User/getuser')
// app.get('/getuser',tokenVerify,getuser)



// var deleteuser = require('./User/deleteusers')
// app.delete('/deleteuser',deleteuser.deleteuser)



var addadmin = require('./Admin/addadmins')
app.post('/addadmin',tokenVerify,addadmin)


var getadmin = require('./Admin/getadmins')
app.get('/getadmin',tokenVerify,getadmin.getadmin)

// var getadmin = require('./Admin/getadmins')
// app.get('/getadmin',tokenVerify,getadmin)


var updateadmin = require('./Admin/updateadmins')
app.post('/updateadmin',tokenVerify,updateadmin)


var deleteadmin = require('./Admin/deleteadmin')
app.delete('/deleteadmin',tokenVerify,deleteadmin.deleteadmin)

// var deleteadmin = require('./Admin/deleteadmin')
// app.delete('/deleteadmin',tokenVerify,deleteadmin)



// var gettaskkss = require('./Admin/getadmins')
// app.get('/gettaskkss',gettaskkss.gettaskkss)


// var getspectask = require('./User/getuser')
// app.get('/getspectask',getspectask)


var register = require('./register')
app.post('/register',register.register)

var login = require('./login')
app.post('/login',login.login)


module.exports = app;