const mongoose = require('mongoose')
const Schema =  mongoose.Schema

var newTask = new Schema({
    Name:String,
    Emailid:{
        type:String,
        unique:true
    },
    Password:String,
    CreatedBy: mongoose.Schema.Types.ObjectId,
    status : Number
})

module.exports=mongoose.model('taskthree',newTask)